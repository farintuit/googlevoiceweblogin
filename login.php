<?php 
$redirect_uri = '';
$state = '';
$DEV = '';
extract($_GET);
extract($_POST);

$text = "";
if(isset($DEV) && $DEV ==1)
    $text = "(Development)";

//include "base_include.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Sitterfirends app login</title>
<link rel="icon" href="http://www.sitterfriends.com/main/images/favicon.png">
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->



<link href="http://designers.hubspot.com/hs-fs/hub/327485/file-2054199286-css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap Core CSS -->
<link href="css/style-login.css" rel="stylesheet">
<style>
	#otp-varify{margin-top: 20px;}
	.displaynone{display:none;}	
	.displayblock{display:block;}
	.has-error{color: red;}
	.has-sucess{color: forestgreen;}
.sso_title {
	font-size: 18px;
	font-weight: 500;
	padding-bottom: 8px;
	letter-spacing: -0.4px; text-align: center;
}
.sso_sub-title {
	opacity: 0.5;
	font-size: 14px;
	line-height: 1.21;
	letter-spacing: -0.2px; text-align: center;
}
.phone-number-wrapper{
	position: relative;
	margin-top: 16px; margin-bottom: 40px;
	height: 48px;
	border-radius: 4px;
	background-color: #ffffff;
	border: solid 1px #e5e5e5;
}
input{
	display: inline-block;
	vertical-align: middle;
	opacity: 0.5;
	font-size: 16px;
	letter-spacing: -0.3px;
	height: 44px;
	border-radius: 4px;
	background-color: #ffffff;
	border: none;
	width: 75%;
	padding-left: 8px;
	outline: none;
}
	.logo{margin-bottom: 20px;}
	.logo img{max-width: 120px;}
	.help-block{position: absolute; top: 45px;}
	body{
		
		font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
	}
	.disableCls{background-color: #E6E6E6;}
	
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before{
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.outer {
    display: table;
    position: absolute;
    height: 100%;
    width: 100%;
}

.middle {
    display: table-cell;
    vertical-align: middle;
}

.login-form {
    margin-left: auto;
    margin-right: auto; 
   
}	
@media only screen and (max-width: 400px) {
	.login-form { margin: auto 8px;}
}
</style>
<style>
.orverlay-loader {
    background: rgba(0,0,0,0.7);
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    z-index: 10;
}	
.loader {
    position: absolute;
    left: 50%;
    top: 50%;
    margin: -32px 0px 0px -32px;
}
.loader {
  border: 10px solid #f3f3f3;
  border-radius: 50%;
  border-top: 10px solid #3498db;
  width: 80px;
  height: 80px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
</head>

<body>

<div class="outer">
  <div class="middle">
    <div class="login-form">
     <div class="logo">
     	
     	<a href="http://www.sitterfriends.com/">
    <img src="images/wp_logo.png" class="img-responsive center-block" title="logo" alt="logo"></a>
     </div>
     <div id="login-form-box">
      <div class="sso_title">Enter your mobile number<?=$text;?></div>
      <div class="sso_sub-title">A 4-digit OTP will be sent on SMS</div>
       <form class="" onSubmit="return false;"  name="login-form" id="login-form" method="POST">
        <div class="col-md-12">
          
          <div class="text-center" id="msg-div"></div>
          <div class="form-group phone-number-wrapper">
            
            <input type="text" name="country_code" value="+1" class="col-lg-3 col-md-3 col-sm-3 col-xs-4" style="border-right: solid 1px #ebebeb;" />
            <input name="number" class="form-control1 col-lg-9 col-md-9 col-sm-9 col-xs-8" id="number" type="text" placeholder="10 digit mobile number">
          </div>
          <input type="hidden" name="DEV" value="<?php if(isset($DEV) && $DEV==1){ echo $DEV; }?>" />
          <input type="hidden" name="usertype" value="parent" />
          <input type="hidden" name="SEND_SMS" value="0" />
          <input type="hidden" name="SIGNUP" value="0" />
          <input type="hidden" name="action" value="do-login" />
        </div>
        <br />
        <div class="col-md-12">
          <button type="submit" id="mobile-number" class="btn btn-default">Next</button>
          
          
        </div>
      </form>
      </div>
      <div id="otp-form-box" class="displaynone">
       <div class="sso_title">Verify and login<?=$text;?></div>
      <div class="sso_sub-title">Enter the OTP sent to your mobile 7023920009</div>
      <form  onSubmit="return false;"  name="otp-form" id="otp-form" method="POST">
        <div class="col-md-12">
          
      <div class="text-center" id="msg-otp-div"></div>
      <!--  <div class="form-group">
            <label for="phone">Mobile Number:</label>
            <input type="tel" class="form-control" id="phone">
          </div>-->
          <div class="form-group">
            <input type="text" class="form-control" id="user_otp" name="user_otp" placeholder="OTP">
          </div>
        </div>
        <div class="col-md-12">
         <input type="hidden" name="DEV" value="<?php if(isset($DEV) && $DEV==1){ echo $DEV; } ?>" />
          <input type="hidden" name="action" value="do-otp-varify" />
          <input type="hidden" name="user_phone" id="user_phone" />
          <button type="submit" class="btn btn-default" id="otp-varify">Verify</button>
          
        </div>
      </form>
      <a class="resend-link" href="#" id="otp-resend" >Resend</a>
	</div>
    </div>
  </div>
</div>
<!-- /#page-wrapper --> 

<!-- Modal -->
  <div class="modal" id="popupModel" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
          <h4 class="modal-title">SitterFriends app </h4>
        </div>
        <div class="modal-body">
          <p style="text-align: center">Invoke SitterFriends app in google assistant by saying -<br/>"Talk to SitterFriends".</p>
        </div>
        <div class="modal-footer">
          <button type="button" id="popupok" class="btn btn-default" data-dismiss="modal">Continue</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- jQuery --> 
<script src="js/jquery.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>

<script>
	
$(document).ready(function(){
    
$('#login-form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
           $("#pre-loader").removeClass("displaynone").addClass("displayblock");
			$.ajax({
					type: 'post',
					url: 'login-code.php',
					dataType:'json',
					data: $('#login-form').serialize(),
					success: function (result) {
						 $.each(result, function(index, item) {
						 	if(item.status==0){
							    $("#msg-div").html(item.msg);
								$("#msg-div").removeClass('has-sucess').addClass("has-error");
							 }else if(item.status==1){
								 $("#user_phone").val(item.PHONE);
								 $(".sso_sub-title").html(item.msg);
								 $(".sso_sub-title").removeClass('has-error').addClass("has-sucess");
								 $("#login-form-box").addClass("displaynone");
								 $("#otp-form-box").removeClass("displaynone").addClass("displayblock");
							 }
							 
						 
						 });
						 $("#pre-loader").removeClass("displayblock").addClass("displaynone");
					 }
				});
      },
	fields: {
            
            number: {
                validators: {
                    notEmpty: {
                        message: 'Please enter mobile number'
                    },
                    stringLength: {
                           message: 'Please enter correct mobile number',
                           max:10,
                           min:10
                    },integer: {
                         message: 'The value is not an integer',

                    }
                }
            }
            }
        });
	
$('#otp-form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {
           $("#pre-loader").removeClass("displaynone").addClass("displayblock");
			$.ajax({
                type: 'post',
                url: 'login-code.php',
                dataType:'json',
               // data: $('#otp-form').serialize()+'&user_name='+$("#user_name").val()+'&user_pwd='+$("#user_pwd").val(),
		 		data: $('#otp-form').serialize(),
                success: function (result) {
                     $.each(result, function(index, item) {
                     if(item.status==1){
						 $("#popupModel").modal({backdrop: 'static', keyboard: false},'show');
						 setTimeout(function() {
							 //$('#popupModel').modal('hide');
							 $( "#popupok" ).click();
						 }, 5000);
						 $( "#popupok" ).click(function() {
							var redirectUrl = '<?=$redirect_uri;?>#access_token='+item.token+'&token_type=bearer&state=<?=$state;?>';
						 //alert(redirectUrl);
						 window.location=redirectUrl;

						});

                     }else if(item.status==0){
                           $(".sso_sub-title").html(item.msg);
                            $(".sso_sub-title").removeClass('has-sucess').addClass("has-error");
                     }
                     });
                   $("#pre-loader").removeClass("displayblock").addClass("displaynone");
                 }
            });
			
      },
	fields: {
            
            user_otp: {
                validators: {
                    notEmpty: {
                        message: 'Please enter OTP number'
                    },
                    stringLength: {
                           message: 'Please enter correct OTP number',
                           max:4,
                           min:4
                    },integer: {
                         message: 'The value is not an integer',

                    }
                }
            }
            }
        });
	
	
});	
/*$( "#mobile-number" ).click(function() {
	$.ajax({
					type: 'post',
					url: 'login-code.php',
					dataType:'json',
					data: $('#login-form').serialize(),
					success: function (result) {
						 $.each(result, function(index, item) {
						 
							 $("#login-form").addClass("displaynone");
							 $("#otp-form").removeClass("displaynone").addClass("displayblock");
						 
						 });
						 //$("#pre-loader").removeClass("displayblock").addClass("displaynone");
					 }
				});
	
	
	
});*/
	$( "#otp-resend" ).click(function() {
	$("#pre-loader").removeClass("displaynone").addClass("displayblock");
	$.ajax({
					type: 'post',
					url: 'login-code.php',
					dataType:'json',
					data: $('#login-form').serialize(),
					success: function (result) {
						 $.each(result, function(index, item) {
						 	if(item.status==0){
							    $("#msg-div").html(item.msg);
								$("#msg-div").removeClass('has-sucess').addClass("has-error");
							 }else if(item.status==1){
								 $("#user_phone").val(item.PHONE);
								 $(".sso_sub-title").html(item.msg);
								 $(".sso_sub-title").removeClass('has-error').addClass("has-sucess");
								 $("#login-form-box").addClass("displaynone");
								 $("#otp-form-box").removeClass("displaynone").addClass("displayblock");
							 }
							 
						 
						 });
						 $("#pre-loader").removeClass("displayblock").addClass("displaynone");
					 }
				});
	});
	
</script>

<div class="orverlay-loader displaynone" id="pre-loader">
    <div class="loader"></div>
</div>
</body>
</html>
